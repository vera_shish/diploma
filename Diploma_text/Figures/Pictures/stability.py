import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':22})
import numpy as np
import pandas as pd
import sklearn
from scipy.interpolate import make_interp_spline
from sklearn.metrics import pairwise_distances

def get_places(emb, stab_inns, postfix1='_r50_1', postfix2='_r50_2'):
    """
    Counts place of second part of firm in lists of nearests of first part of firm for each firm in stab_inns, 
    which has been splited in two firms.
    
    Parameters:
    -----------
    emb: pandas DataFrame,
         embeddings, index - inns, rows - embeddings
    stab_inns: list of strings,
               inns of firms which has been splited in two parts
    postfix1: str,
              postfix for inn in one part of firm ((inn of the part of firm) = (inn from the stab_inns) + (postfix))
    postfix2: str,
              postfix for inn in other part of firm ((inn of the part of firm) = (inn from the stab_inns) + (postfix))
              
    Returns:
    --------
    dict: keys - inns from stab_inns, values - places in list of second parts
    dict: keys - inns from stab_inns, values - distances between parts of firm with inn from stab_inns
    """
    places = {}
    distances = {}
    new_inns = list(map(lambda x: x+postfix1, stab_inns))
    emb_target = emb[emb.index.isin(new_inns)]
    dist = pairwise_distances(emb.values, emb_target, metric='cosine', n_jobs=-1)
    dists = pd.DataFrame(dist, index=emb.index, columns=emb_target.index)
    for inn in new_inns:
        old_inn = inn.split('_', 1)[0]
        pair_inn = old_inn + postfix2
        
        dists_inn = dists[inn].sort_values()
        place = list(dists_inn.index).index(pair_inn)
        dst = dists_inn[pair_inn]
        places[old_inn] = place
        distances[old_inn] = dst
    return places, distances


def get_dists(emb, inns):
    """
    Count dists between new pseudo-firms and their prototypes.
    
    Parameters
    emb: pandas DataFrame, index - inn, rows - embeddings
    inns: list of strings, prototype inns
    
    Returns
    pandas DataFrame: distances between new pseudo-firms and their prototypes, columns=['inn1', 'inn2', 'dist']
    """
    table = pd.DataFrame(columns=['inn1', 'inn2', 'dist'])
    for inn in inns:
        emb_tmp = emb[emb.index.map(lambda x: x.startswith(inn))].copy()
        dists = pd.DataFrame(sklearn.metrics.pairwise_distances(emb_tmp[emb_tmp.index != inn].values,
                                                                emb_tmp.loc[inn, :].values.reshape(1, -1),
                                                                metric='cosine'),
                             index=emb_tmp[emb_tmp.index != inn].index, columns=['dist']).reset_index(drop=False)
        dists['inn1'] = inn
        if 'inn' in dists.columns:
            dists.rename(columns={'inn': 'inn2'}, inplace=True)
        else:
            dists.rename(columns={'index': 'inn2'}, inplace=True)
        table = pd.concat([table, dists], axis=0, sort=False)

        inn_rand_1 = '{}_r50_1'.format(inn)
        inn_rand_2 = '{}_r50_2'.format(inn)
        dists_rand = float(sklearn.metrics.pairwise_distances(emb_tmp.loc[inn_rand_1, :].values.reshape(1, -1),
                                                              emb_tmp.loc[inn_rand_2, :].values.reshape(1, -1),
                                                              metric='cosine'))
        table = pd.concat([table, pd.DataFrame([[inn_rand_1, inn_rand_2, dists_rand]],
                                               columns=['inn1', 'inn2', 'dist'],
                                               index=[0])], axis=0, sort=False)

        inn_half_1 = '{}_c50_1'.format(inn)
        inn_half_2 = '{}_c50_2'.format(inn)
        dists_half = float(sklearn.metrics.pairwise_distances(emb_tmp.loc[inn_half_1, :].values.reshape(1, -1),
                                                              emb_tmp.loc[inn_half_2, :].values.reshape(1, -1),
                                                              metric='cosine'))
        table = pd.concat([table, pd.DataFrame([[inn_half_1, inn_half_2, dists_half]],
                                               columns=['inn1', 'inn2', 'dist'],
                                               index=[0])], axis=0, sort=False)
    table.reset_index(drop=True, inplace=True)
    return table


def plot_split(dists, inns, path=None, show_xticks=True):
    plt.figure(1, figsize=(10, 8))

    y_rand = []
    for inn in inns:
        inn_index = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_r50'))) &
                          (dists['inn2'].map(lambda x: x.startswith(inn + '_r50')))].index
        y_rand.append(float(dists.loc[inn_index, 'dist']))
    tmp = sorted(zip(inns, y_rand), key=lambda x: x[1])
    x = [i[0] for i in tmp]
    y = [i[1] for i in tmp]
    plt.plot(range(len(x)), y, '-o', linewidth=0, label='distance between random halfs')

    y_half = []
    for inn in inns:
        inn_index = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_c50'))) &
                          (dists['inn2'].map(lambda x: x.startswith(inn + '_c50')))].index
        y_half.append(float(dists.loc[inn_index, 'dist']))
    tmp = list(zip(inns, y_half))
    y1 = []
    for xi in x:
        for item in tmp:
            if item[0] == xi:
                y1.append(item[1])
                break
    plt.plot(range(len(x)), y1, '-o', linewidth=0, label='distance between mediana halfs')
    
    if show_xticks:
        plt.xticks(range(len(x)), x, rotation=90)

    plt.title('Stability')
    plt.xlabel('Firms')
    plt.ylabel('Cosine distance')
    plt.grid()
    plt.legend()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')
    plt.show()


def plot_percent(dists, inns, percents, path=None, show_xticks=True):
    plt.figure(1, figsize=(10, 8))
    x = None
    for per in percents:
        y_per = []
        for inn in inns:
            inn_index = dists[(dists['inn1'] == '{0}_r{1}'.format(inn, per)) |
                              (dists['inn2'] == '{0}_r{1}'.format(inn, per))].index
            y_per.append(float(dists.loc[inn_index, 'dist']))
        if x is None:
            tmp = sorted(zip(inns, y_per), key=lambda x: x[1])
            x = [i[0] for i in tmp]
            y = [i[1] for i in tmp]
        else:
            tmp = list(zip(inns, y_per))
            y = []
            for xi in x:
                for item in tmp:
                    if item[0] == xi:
                        y.append(item[1])
                        break
        plt.plot(range(len(x)), y, '-o', linewidth=0, label='{}% removed'.format(per))

    if show_xticks:
        plt.xticks(range(len(x)), x, rotation=90)

    plt.title('Stability')
    plt.xlabel('Percent')
    plt.ylabel('Cosine distance')
    plt.grid()
    plt.legend()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')
    plt.show()


def plot_parts(dists, inns, path=None, show_xticks=True):
    plt.figure(1, figsize=(10, 8))
    x = None
    for end in ['_r50_1', '_r50_2', '_c50_1', '_c50_2']:
        y_onehalf = []
        for inn in inns:
            inn_index = dists[((dists['inn1'] == inn) & (dists['inn2'] == inn + end)) |
                              ((dists['inn1'] == inn + end) & (dists['inn2'] == inn))].index
            y_onehalf.append(float(dists.loc[inn_index, 'dist']))
        if x is None:
            tmp = sorted(zip(inns, y_onehalf), key=lambda x: x[1])
            x = [i[0] for i in tmp]
            y = [i[1] for i in tmp]
        else:
            tmp = list(zip(inns, y_onehalf))
            y = []
            for xi in x:
                for item in tmp:
                    if item[0] == xi:
                        y.append(item[1])
                        break
        plt.plot(range(len(x)), y, '-o', linewidth=0, label='main with {}'.format(end[1:]))

    if show_xticks:
        plt.xticks(range(len(x)), x, rotation=90)

    plt.title('Stability')
    plt.xlabel('Firms')
    plt.ylabel('Cosine distance')
    plt.grid()
    plt.legend()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')
    plt.show()


def plot_mean(dists, inns, percents, path=None):
    data = []
    plt.figure(1, figsize=(10, 8))

    y_rand = []
    for inn in inns:
        inn_index = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_r50'))) &
                          (dists['inn2'].map(lambda x: x.startswith(inn + '_r50')))].index
        y_rand.append(float(dists.loc[inn_index, 'dist']))
    y_rand = np.array(y_rand).mean()
    data.append(('random halfs', y_rand))

    y_half = []
    for inn in inns:
        inn_index = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_c50'))) &
                          (dists['inn2'].map(lambda x: x.startswith(inn + '_c50')))].index
        y_half.append(float(dists.loc[inn_index, 'dist']))
    y_half = np.array(y_half).mean()
    data.append(('ordered halfs', y_half))

    for per in percents:
        y_per = []
        for inn in inns:
            inn_index = dists[(dists['inn1'] == '{0}_r{1}'.format(inn, per)) |
                              (dists['inn2'] == '{0}_r{1}'.format(inn, per))].index
            y_per.append(float(dists.loc[inn_index, 'dist']))
        y_per = np.array(y_per).mean()
        data.append(('{}% removed'.format(per), y_per))

    for end in ['_r50_1', '_r50_2']:
        y_onehalf = []
        for inn in inns:
            inn_index = dists[((dists['inn1'] == inn) & (dists['inn2'] == inn + end)) |
                              ((dists['inn1'] == inn + end) & (dists['inn2'] == inn))].index
            y_onehalf.append(float(dists.loc[inn_index, 'dist']))
    y_onehalf = np.array(y_onehalf).mean()
    data.append(('main with random halfs', y_onehalf))

    for end in ['_c50_1', '_c50_2']:
        y_onehalf = []
        for inn in inns:
            inn_index = dists[((dists['inn1'] == inn) & (dists['inn2'] == inn + end)) |
                              ((dists['inn1'] == inn + end) & (dists['inn2'] == inn))].index
            y_onehalf.append(float(dists.loc[inn_index, 'dist']))
    y_onehalf_ = np.array(y_onehalf).mean()
    data.append(('main with ordered halfs', y_onehalf_))

    plt.plot(range(len(data)), [item[1] for item in data], '-o', markersize=14, linewidth=0)
    plt.xticks(range(len(data)), [item[0] for item in data], rotation=90)

    plt.title('Stability')
    plt.xlabel('Difference')
    plt.ylabel('Mean cosine distance')
    plt.grid()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')
    plt.show()


def plot_split_sim(dists, inns, path=None):
    fig, ax = plt.subplots(1, 1, figsize=(10, 8))
    y_rand = []
    y_half = []
    for inn in inns:

        inn_index1 = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_r50_'))) &
                           (dists['inn2'].map(lambda x: x.startswith(inn + '_r50_')))].index

        inn_index = dists[(dists['inn1'].map(lambda x: x.startswith(inn + '_c50_'))) &
                          (dists['inn2'].map(lambda x: x.startswith(inn + '_c50_')))].index
        try:
            y_half.append(float(dists.loc[inn_index, 'dist']))
            y_rand.append(float(dists.loc[inn_index1, 'dist']))
        except:
            pass

    plt.plot(y_rand, y_half, '-o', linewidth=0)
    x = np.linspace(0, max(y_rand) + 0.1, 102)
    plt.plot(list(x), list(x), '--', linewidth=1)
    ax.set_yscale('log')
    ax.set_xscale('log')
    plt.title('Stability')
    plt.xlabel('Cosine distance between random halfs')
    plt.ylabel('Cosine distance between ordered halfs')
    plt.grid()
    plt.legend()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')
    plt.show()


def count_trans(trans, inns, fields=None):
    """
    Get number of transaction for firms.
    
    Parameters
    trans: pandas DataFrame, loaded transactions
    inns: list of str, list of inns
    fields: list of column names in trans dataframe with inns by which it's needed to count
    
    Returns
    pandas DataFrame: index - inns, column - number of transactions
    """
    if fields is None:
        fields = ['c_kl_kt_2_inn', 'c_kl_dt_2_inn']

    table = pd.DataFrame(0, index=inns, columns=['count'])
    for field in fields:
        table_tmp = trans[trans[field].isin(inns)].groupby(field)[field].agg(['count'])
        table['count'] = table['count'] + table_tmp.loc[inns, 'count'].fillna(0)

    return table


def count_quantiles(points, k=100):
    """
    Prameters
    ---------
    points: list of lists
        points - [[x1,y1],[x2,y2],...]
    k: int
        number of points for obtain point in mean plot
        
    Returns:
    --------
    pandas DtaFrame, 
        counted statistics with averaging by k points
    """
    points = pd.DataFrame(points).sort_values(0)
    
    df = pd.DataFrame(columns = ['Number of transactions', 'mean','25%','50%','75%'])
    
    for i in range(round(points.shape[0]/k)):
        if i * k + k > points.shape[0]-1:
            top = points.shape[0]-1
        else:
            top = i * k + k 
        df_tmp = points.loc[points.index[i * k:top], :]
        axx = 0
        df_ = pd.DataFrame({'mean':df_tmp[1].mean(axis=0), 
                           'Number of transactions': df_tmp[0].mean(axis=0),
                           '25%':df_tmp.quantile(0.25,axis=0)[1], 
                           '50%':df_tmp.quantile(0.50,axis=0)[1], 
                           '75%':df_tmp.quantile(0.75,axis=0)[1]}, index=[1])
        df = pd.concat([df, df_], axis=0, sort=False)
    df.set_index('Number of transactions', inplace=True, drop=True)
    return df


def plot_split_std_rand(dists, sorted_inns, path=None, k=10, log=False):
    """
    Displays companies sorted by number of transactions, which was splited in two parts, and cosine distance beetween parts for each company.
    
    Prameters
    ---------
    dists: pandas DataFrame, 
        distances between new pseudo-firms and their prototypes, columns=['inn1', 'inn2', 'dist']
    sorted_inns: pandas DataFrame,
        index - inns, column - number of transactions
    path: str,
        where to save picture
    k: int
        number of points for obtain point in mean plot
    log: bool
        use logarithmical scale in axes.
        
    """
    
    fig, ax = plt.subplots(1, 1, figsize=(20, 10))
    y_rand = []
    for inn in sorted_inns.index:
        inn_index = dists[(dists['inn1'].str.startswith(inn + '_r50_')) &
                          (dists['inn2'].str.startswith(inn + '_r50_'))].index
        y_rand.append((sorted_inns.loc[inn, 'count'], float(dists.loc[inn_index, 'dist'])))
        
    df = count_quantiles(y_rand, k=k)
    plt.scatter([item[0] for item in y_rand],[item[1] for item in y_rand], alpha=0.5)
    plt.title('Stability')
    plt.xlabel('Number of transactions')
    plt.ylabel('Cosine distance between random halfs')
    plt.grid()

    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')

    if log:
        ax.set_yscale('log')
        ax.set_xscale('log')
        
    df.plot(ax=ax, linewidth=2)
    
    if path is not None:
        plt.savefig(path,
                    additional_artists=[],
                    bbox_inches='tight')